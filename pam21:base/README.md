# PAM @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/pam21:base:**
* Ordre per executar el container:
* Xarxa propia:

```

docker build -t isx25633105/pam21:base .

docker run --privileged --rm --name pam.edt.org -h pam.edt.org --net 2hisx -it isx25633105/pam21:base


```
* Exercicis fets:
*Exercici1:*
fitxer /etc/pam.d/chfn

```
account optional pam_echo.so [type:account rhost: %H lhost; %h service: %s terminal: %t ruser: %U user: %u]
account optional pam_echo.so [entra al time]
account sufficient pam_time.so debug
account optional pam_echo.so [surt del time]
account required pam_deny.so
```
fitxer /etc/security/time.conf

```
#1
chfn;*;*;Al0800-1000;1600-1800
#2
chfn;*;*;!Al0800-1000;!1600-1800
#3
chfn;*;unix01;Al0800-1400
chfn;*;*;!Al0000-2400
#4
chfn;*;unix01;Wk0000-2400
chfn;*;unix02;Al1300-0800
```
*Exercici 2:*
fitxer /etc/security/pam_mount.conf.xml
```
<volume
       user="*"
       fstype="tmpfs"
       mountpoint="~/mytmp"
       options="size=100M" 
     />
```
```
<volume
       user="unix01"
       fstype="tmpfs"
       mountpoint="~/mytmp"
       options="size="200M" 
     />
```
```
<volume
       user="unix02"
       fstype="nfs4"
       server="ip"
       path="/var/tmp/compartir"
       mountpoint="~/nfs" 
     />


