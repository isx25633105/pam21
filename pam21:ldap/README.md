# PAM @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/pam21:ldap:**
* Ordre per executar el container:
* Xarxa propia:

```

docker build -t isx25633105/pam21:ldap .

docker run --privilieged --rm --name pam.edt.org -h pam.edt.org --net 2hisx -it isx25633105/pam21:ldap
```

S'ha modificat la versio de pam base per poder identificarse en una container ldap(isx25633105/ldap21:group.
